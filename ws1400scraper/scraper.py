# -*- coding: utf-8 -*-

import argparse
import time
import logging
from influxdb import InfluxDBClient
import schedule
from ws1400scraper.config import WSFileConfig, WSEnvConfig
from ws1400scraper.measurement import WSMeasurement
from ws1400scraper.control import write_measurement

def job(config):
    dbclient = InfluxDBClient(
            config.influx_host,
            config.influx_port,
            config.influx_username,
            config.influx_password,
            config.influx_database,
            config.influx_ssl,
            config.influx_verify_ssl,
            config.influx_timeout,
            config.influx_retries,
            config.influx_use_udp,
            config.influx_udp_port
        )
    measurement = WSMeasurement(config.data_url, config.unit_url)
    write_measurement(dbclient, measurement, config.location)
    logging.info("Wrote a new measurement to the database")
    logging.debug("Measurement: %s" % measurement.to_dict())

def daemon(config):
    schedule.every(config.interval).seconds.do(job, config)

    while True:
        schedule.run_pending()
        logging.debug("Sleeping 1 second")
        time.sleep(1)

def main():
    description = ("A program to scrape a ws1400-ip weather station and " +
                   "store the measurement in an InfluxDB")
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-l', '--loglevel', default='INFO')
    parser.add_argument('-r', '--runonce', action='store_true',
                        help="If specified, does not run on a schedule")
    parser.add_argument('-c', '--configfile',
                        help="path to config file")
    args = parser.parse_args()

    numeric_level = getattr(logging, args.loglevel.strip().upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: `%s`' % args.loglevel.strip())
    logging.basicConfig(level=numeric_level)

    logging.info("Loading configuration from `%s`" % args.configfile)
    if args.configfile:
        config = WSFileConfig(args.configfile)
    else:
        config = WSEnvConfig()

    if args.runonce:
        logging.info("Executing parsing job once")
        job(config)
    else:
        logging.info("Scheduling parser job every %d seconds" % config.interval)
        daemon(config)
