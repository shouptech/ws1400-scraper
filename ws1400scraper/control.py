# -*- coding: utf-8 -*-

from datetime import datetime

def write_measurement(dbclient, measurement, location):
    """Writes the measurement to the database

    Arguments:
    - dbclient: An InfluxDB client
    - measurement: A WSMeasurement object
    - location: String specifying location of the station"""

    json_body = [{
        "measurement": "weather",
        "tags": {
            "location": location,
            "station_url": measurement.data_url
        },
        "time": int(datetime.now().timestamp()*1000000),
        "fields": {
            "outdoor_temp": float(measurement.outdoor_temp),
            "outdoor_humidity": float(measurement.outdoor_humidity),
            "wind_direction": float(measurement.wind_direction),
            "average_wind_speed": float(measurement.average_wind_speed),
            "gust_speed": float(measurement.gust_speed),
            "solar_radiation": float(measurement.solar_radiation),
            "uv": float(measurement.uv),
            "uv_index": float(measurement.uv_index),
            "hourly_rain": float(measurement.hourly_rain),
            "daily_rain": float(measurement.daily_rain),
            "weekly_rain": float(measurement.weekly_rain),
            "monthly_rain": float(measurement.monthly_rain),
            "yearly_rain": float(measurement.yearly_rain)
        }
    }]

    # Check these individually. A 0 value means the indoor sensor was not
    # connected, so we're ignoring the values
    try:
        json_body[0]['fields']['indoor_temp'] = float(measurement.indoor_temp)
    except ValueError:
        print('Invalid value indoor_temp')
        pass
    try:
        json_body[0]['fields']['indoor_humidity'] = \
            float(measurement.indoor_humidity)
    except ValueError:
        print('Invalid value indoor_humidity')
        pass
    try:
        json_body[0]['fields']['absolute_pressure'] = \
            float(measurement.absolute_pressure)
        json_body[0]['fields']['relative_pressure'] = \
            float(measurement.relative_pressure)
    except ValueError:
        print('Invalid value for pressure')
        pass

    dbclient.write_points(json_body, time_precision='u')
