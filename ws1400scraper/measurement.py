# -*- coding: utf-8 -*-

from .parser import WSDataParser, WSUnitParser

def to_mph(value, unit):
    if unit == 'mph':
        return value
    elif unit == 'm/s':
        return value * 2.2369
    elif unit == 'km/h':
        return value * 0.6213
    elif unit == 'ft/s':
        return value * 0.6818
    elif unit == 'bft':
        return value * 1.87
    elif unit == 'knot':
        return value * 1.1507
    else:
        return None

def to_in(value, unit):
    if unit == 'in':
        return value
    elif unit == 'mm':
        return value * 0.03937
    else:
        return None

def to_inhg(value, unit):
    if unit == 'inhg':
        return value
    elif unit == 'hpa':
        return value * 0.0002953
    elif unit == 'mmhg':
        return value * 0.03937
    else:
        return None

def to_degf(value, unit):
    if unit == 'degF':
        return value
    elif unit == 'degC':
        return value * 1.8 + 32

def to_w_per_m2(value, unit):
    if unit == 'w/m2':
        return value
    elif unit == 'lux':
        return value * 0.0079
    elif unit == 'fc':
        return value * .0850
    else:
        return None

class WSMeasurement():
    """A simple class to represent measurements from a weather station

    Attributes:

        indoor_temp
        indoor_humidity
        absolute_pressure
        relative_pressure
        outdoor_temp
        outdoor_humidity
        wind_direction
        average_wind_speed
        gust_speed
        solar_radiation
        uv
        uv_index
        hourly_rain
        daily_rain
        weekly_rain
        monthly_rain
        yearly_rain
    """
    def __init__(self, data_url, unit_url):
        self.data_url = data_url
        self.unit_url = unit_url

        data = WSDataParser(self.data_url)
        units = WSUnitParser(self.unit_url)

        self.indoor_temp = to_degf(data.data['inTemp'], units.unit_temp)
        self.indoor_humidity = data.data['inHumi']
        self.absolute_pressure = to_inhg(
            data.data['AbsPress'], units.unit_pressure)
        self.relative_pressure = to_inhg(
            data.data['RelPress'], units.unit_pressure)
        self.outdoor_temp = to_degf(data.data['outTemp'], units.unit_temp)
        self.outdoor_humidity = data.data['outHumi']
        self.wind_direction = data.data['windir']
        self.average_wind_speed = to_mph(data.data['avgwind'], units.unit_wind)
        self.gust_speed = to_mph(data.data['gustspeed'], units.unit_wind)
        self.solar_radiation = to_w_per_m2(
            data.data['solarrad'], units.unit_solar)
        self.uv = data.data['uv']
        self.uv_index = data.data['uvi']
        self.hourly_rain = to_in(data.data['rainofhourly'], units.unit_rainfall)
        self.daily_rain = to_in(data.data['rainofdaily'], units.unit_rainfall)
        self.weekly_rain = to_in(data.data['rainofweekly'], units.unit_rainfall)
        self.monthly_rain = to_in(
            data.data['rainofmonthly'], units.unit_rainfall)
        self.yearly_rain = to_in(data.data['rainofyearly'], units.unit_rainfall)

    def __repr__(self):
        return("%s('%s', '%s')" % (
               self.__class__.__name__, self.data_url, self.unit_url))

    def to_dict(self):
        return ({ 'indoor_temp' : self.indoor_temp,
                  'inddor_humidity' : self.indoor_humidity,
                  'absolute_pressure' : self.absolute_pressure,
                  'relative_pressure' : self.relative_pressure,
                  'outdoor_temp' : self.outdoor_temp,
                  'outdoor_humidity' : self.outdoor_humidity,
                  'wind_direction' : self.wind_direction,
                  'average_wind_speed' : self.average_wind_speed,
                  'gust_speed' : self.gust_speed,
                  'solar_radiation' : self.solar_radiation,
                  'uv' : self.uv,
                  'uv_index' : self.uv_index,
                  'hourly_rain' : self.hourly_rain,
                  'daily_rain' : self.daily_rain,
                  'weekly_rain' : self.weekly_rain,
                  'monthly_rain' : self.monthly_rain,
                  'yearly_rain' : self.yearly_rain })
