# -*- coding: utf-8 -*-
"""
Contains a class useful for scraping data from an Ambient Weather station
live data page.

Requires the requests package http://python-requests.org/
"""

from html.parser import HTMLParser
import requests

class WSDataParser(HTMLParser):
    """
    Parses a weather station live data page for data and stores it in a
    dictionary (data).

    Arugments:
    - url: URL of the live data page, e.g., http://1.2.3.4/livedata.htm

    Attributes:
    - data: A dictionary containing the measurements
    """

    WS_FIELDS = (
        'inTemp',
        'inHumi',
        'AbsPress',
        'RelPress',
        'outTemp',
        'outHumi',
        'windir',
        'avgwind',
        'gustspeed',
        'solarrad',
        'uv',
        'uvi',
        'rainofhourly',
        'rainofdaily',
        'rainofweekly',
        'rainofmonthly',
        'rainofyearly',
        )

    def __init__(self, url):
        HTMLParser.__init__(self)
        self.data = dict(
            zip(WSDataParser.WS_FIELDS, (None,) * len(WSDataParser.WS_FIELDS)))
        self.feed(requests.get(url).text)
        self.url = url

    def handle_starttag(self, tag, attrs):
        "Parses the page and stores the results in attribute 'data'"
        if tag == 'input':
            dict_attrs = dict(attrs)
            if 'name' in dict_attrs and dict_attrs['name'] in self.data:
                self.data[dict_attrs['name']] = dict_attrs['value']

    def __repr__(self):
        return("%s('%s')" % (self.__class__.__name__, self.url))

class WSUnitParser(HTMLParser):
    """
    Parses a weather station config page and retrieves the units used

    Arugments:
    - url: URL of the config page, e.g., http://1.2.3.4/station.htm

    Attributes:
    - unit_wind: String representing wind speed units
    - unit_rainfall: String representing rainfall units
    - unit_temp: String representing temperature units
    - unit_pressure: String representing pressure units
    - unit_solar: String represenging solar radiation units
    """

    def __init__(self, url):
        HTMLParser.__init__(self)
        self.url = url
        self.unit_wind = None
        self.unit_rainfall = None
        self.unit_temp = None
        self.unit_pressure = None
        self.unit_solar = None
        self._last_select = None
        self._last_option_selected = None

        self.feed(requests.get(self.url).text)

    def handle_starttag(self, tag, attrs):
        """Parses select and option tags"""
        if tag == 'select':
            if 'name' in dict(attrs):
                self._last_select = dict(attrs)['name']
        elif tag == 'option':
            if 'selected' in dict(attrs):
                self._last_option_selected = True
            else:
                self._last_option_selected = False

    def handle_data(self, data):
        """Parses data for units"""
        if self._last_option_selected and len(data.strip()) > 0:
            if not self.unit_wind and self._last_select == 'unit_Wind':
                self.unit_wind = data.strip()
            elif not self.unit_rainfall and self._last_select == 'u_Rainfall':
                self.unit_rainfall = data.strip()
            elif not self.unit_temp and self._last_select == 'u_Temperature':
                self.unit_temp = data.strip()
            elif not self.unit_pressure and self._last_select == 'unit_Pressure':
                self.unit_pressure = data.strip()
            elif not self.unit_solar and self._last_select == 'unit_Solar':
                self.unit_solar = data.strip()

    def __repr__(self):
        return("%s('%s')" % (self.__class__.__name__, self.url))
