# -*- coding: utf-8 -*-

import os
from configparser import ConfigParser

class WSEnvConfig():
    def __init__(self):
        self.data_url = os.environ['WS_DATA_URL']
        self.unit_url = os.environ['WS_UNIT_URL']
        self.location = os.environ['WS_LOCATION']
        self.interval = int(os.environ.get('WS_INTERVAL', 60))
        self.influx_host = os.environ.get('WS_INFLUX_HOST','localhost')
        self.influx_port = int(os.environ.get('WS_INFLUX_PORT', 8086))
        self.influx_username = os.environ.get('WS_INFLUX_USER', 'root')
        self.influx_password = os.environ.get('WS_INFLUX_PASS', 'root')
        self.influx_database = os.environ['WS_INFLUX_DATABASE']
        self.influx_ssl = os.environ.get('WS_INFLUX_SSL', 'false').lower() in [
            'true', 'yes', 'on']
        self.influx_verify_ssl = os.environ.get(
            'WS_INFLUX_VERIFYSSL', 'false').lower() in [
                'true', 'yes', 'on']
        self.influx_timeout = os.environ.get('WS_INFLUX_TIMEOUT', None)
        if self.influx_timeout == 'None':
            self.influx_timeout = None
        elif self.influx_timeout:
            self.influx_timeout = int(self.influx_timeout)
        self.influx_retries = int(os.environ.get('WS_INFLUX_RETRIES', 3))
        self.influx_use_udp = os.environ.get(
            'WS_INFLUX_USE_UDP', 'false').lower() in ['true', 'yes', 'on']
        self.influx_udp_port = int(os.environ.get('INFLUX_UDP_PORT', 4444))

class WSFileConfig():
    def __init__(self, path):
        config = ConfigParser()
        config.read(path)

        self.data_url = config['station']['data_url']
        self.unit_url = config['station']['unit_url']
        self.location = config['station']['location']

        if 'daemon' in config:
            self.interval = int(config['daemon'].get('interval', 60))
        else:
            self.interval = 60

        self.influx_host = config['influx'].get('host', 'localhost')
        self.influx_port = int(config['influx'].get('port', 8086))
        self.influx_username = config['influx'].get('username', 'root')
        self.influx_password = config['influx'].get('password', 'root')
        self.influx_database = config['influx']['database']
        self.influx_ssl = config['influx'].get('ssl', 'false').lower() in [
            'true', 'yes', 'on']
        self.influx_verify_ssl = config['influx'].get(
            'verify_ssl', 'false').lower() in [
                'true', 'yes', 'on']
        self.influx_timeout = config['influx'].get('timeout', None)
        if self.influx_timeout == 'None':
            self.influx_timeout = None
        elif self.influx_timeout:
            self.influx_timeout = int(self.influx_timeout)
        self.influx_retries = int(config['influx'].get('retries', 3))
        self.influx_use_udp = config['influx'].get(
            'use_udp', 'false').lower() in ['true', 'yes', 'on']
        self.influx_udp_port = int(config['influx'].get('udp_port', 4444))
