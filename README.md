# WS1400 Scraper

[![pipeline status](https://gitlab.com/shouptech/ws1400-scraper/badges/master/pipeline.svg)](https://gitlab.com/shouptech/ws1400-scraper/commits/master) [![coverage report](https://gitlab.com/shouptech/ws1400-scraper/badges/master/coverage.svg)](https://gitlab.com/shouptech/ws1400-scraper/commits/master)


A project to scrape the latest weather data from an Ambient Weather WS-1400-IP and store the results in influxDB.

## Installing

Tested with python 3.6. May work with older python 3.x versions. Will not work with python 2.x.

```bash
python setup.py install
```

## Configuration

Configuration can be passed in either using a file, or from the environment.

See the file `sample_config.ini` for configuration options if using a file.

Environment variables can be:

| Variable              | Required | Default     | Description                                          |
|-----------------------|----------|-------------|------------------------------------------------------|
| `WS_DATA_URL`         | yes      | N/A         | Url to livedata, e.g., `http://1.2.3.4/livedata.htm` |
| `WS_UNIT_URL`         | yes      | N/A         | Url to station, e.g., `http://1.2.3.4/station.htm`   |
| `WS_LOCATION`         | yes      | N/A         | physical location of station, e.g. `Somewhere, CO`   |
| `WS_INTERVAL`         | no       | 60          | Number of seconds between polls                      |
| `WS_INFLUX_HOST`      | no       | `localhost` | Host running influx                                  |
| `WS_INFLUX_PORT`      | no       | 8086        | TCP port for influx                                  |
| `WS_INFLUX_USER`      | no       | `root`      | Username to connect to influx                        |
| `WS_INFLUX_PASS`      | no       | `root`      | Password to connect to influx                        |
| `WS_INFLUX_DATABASE`  | yes      | N/A         | Database to store data in                            |
| `WS_INFLUX_SSL`       | no       | false       | Use SSL when connecting to influx                    |
| `WS_INFLUX_VERIFYSSL` | no       | false       | Verify SSL certificate                               |
| `WS_INFLUX_TIMEOUT`   | no       | `None`      | Timeout for connecting to influx                     |
| `WS_INFLUX_RETRIES`   | no       | 3           | Number of retries                                    |
| `WS_INFLUX_USE_UDP`   | no       | false       | Use UDP when connecting to influx                    |
| `INFLUX_UDP_PORT`     | no       | 4444        | UDP port for influx                                  |

## Running

To run the scraper once (i.e., not on a periodic schedule), execute:

```bash
wsscraper -r path_to_config.ini
```

To run the scraper on a periodic interval (the one defined in your config file), execute:

```bash
wsscraper path_to_config.ini
```

The script also takes the argument `-l LOGLEVEL` where LOGLEVEL is one of

* DEBUG
* INFO
* WARNING
* ERROR
* CRITICAL

See [the python documentation here](https://docs.python.org/3.6/howto/logging.html) for the descriptions of the levels.
