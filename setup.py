import os
from setuptools import setup

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "ws1400scraper",
    version = "0.0.1",
    author = "Mike Shoup",
    author_email = "mike@shouptech.com",
    description = ("An application to scrape data from a WS-1400-IP weather "
        "and publish the data to an influx database"),
    license = "MIT",
    keywords = "influxdb scraper ws1400",
    url = "https://gitlab.com/shouptech/ws1400-scraper",
    packages = ['ws1400scraper'],
    long_description = read('README.md'),
    classifiers = [],
    install_requires = [
        'influxdb',
        'requests',
        'schedule'
    ],
    entry_points = {
        'console_scripts': ['wsscraper=ws1400scraper.scraper:main']
    }
)
