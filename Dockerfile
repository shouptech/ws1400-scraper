FROM python:3

COPY README.md scraper/
COPY setup.py scraper/setup.py
COPY ws1400scraper/*.py scraper/ws1400scraper/

RUN (cd scraper && python setup.py install)

CMD wsscraper -l DEBUG
